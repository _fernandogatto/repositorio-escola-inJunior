export class Pessoa {
    private nome: string;
    private cpf: string;
    private dataNascimento: string;

    constructor(nome: string, cpf: string, dataNasciimento: string) {
        this.nome = nome;
        this.cpf = cpf;
        this.dataNascimento = dataNasciimento;
    }

    // getters and setters

    getNome(): string {
        return this.nome;
    }

    setNome(nome: string) {
        this.nome = nome;
    }

    getCpf(): string {
        return this.cpf;
    }

    setCpf(cpf: string) {
        this.cpf = cpf;
    }

    getDataNascimento(): string {
        return this.dataNascimento;
    }

    setDataNascimento(dataNasciimento: string) {
        this.dataNascimento = dataNasciimento;
    }
}