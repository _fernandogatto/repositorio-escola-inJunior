import {Pessoa} from './Pessoa';

export class Professor extends Pessoa {
    private formacao: string;
    private cargaHoraria: number;

    constructor(nome: string, cpf: string, dataNasciimento: string, formacao: string, cargaHoraria :number) {
        super(nome, cpf, dataNasciimento);
        this.formacao = formacao;
        this.cargaHoraria = cargaHoraria;
    }

    // getters and setters

    getFormacao() {
        return this.formacao;
    }

    setFormacao(formacao: string) {
        this.formacao = formacao;
    }

    getCargaHoraria() {
        return this.cargaHoraria;
    }

    setCargaHoraria(cargaHoraria: number) {
        this.cargaHoraria = cargaHoraria;
    }
}

let professor = new Professor("Rafael", "123-456-123-00", "01/01/1990", "Banco de dados", 80);