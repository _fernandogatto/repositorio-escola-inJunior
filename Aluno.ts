import {Pessoa} from './Pessoa';

export class Aluno extends Pessoa {
    private matricula: string;
    private curso: string;

    constructor(nome: string, cpf: string, dataNasciimento: string, matricula: string, curso: string) {
        super(nome, cpf, dataNasciimento);
        this.matricula = matricula;
        this.curso = curso;
    }

    // getters and setters

    getMatricula() {
        return this.matricula;
    }

    setMatricula(matricula: string) {
        this.matricula = matricula;
    }

    getCurso() {
        return this.curso;
    }

    setCurso(curso: string) {
        this.curso = curso;
    }

    public calcularMedia(p1: number, p2: number): number {
        return (p1 + p2) / 2;
    }
}

let aluno = new Aluno("Fernando", "123-123-123-90", "01/01/2000", "SI123", "SI");
aluno.calcularMedia(7,8);